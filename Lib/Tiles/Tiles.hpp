#include <string>
#include "DirectionnalVector/DirectionnalVector.hpp"
#include "Points/Points.hpp"
#include <fstream>
#include <cstdarg>
#include "array"

template<int dimension>
class Tiles
{
  private:
    std::string beforeSamplingPoint;
    Points<dimension> samplingPoint;
    Points<dimension> previousRefPoint;
    std::array<DirectionnalVector,dimension> previousDirectionnalVectors;
    std::string afterPreviousDirectionnalVectors;
  public:
    inline Tiles();
    // inline void vectorPointSumRand(Points*,Points,double,int);
    inline ~Tiles();

    inline  Points<dimension>* getSamplingPoint();
    inline void setSamplingPoint(Points<dimension>);

    inline Points<dimension>* getPreviousRefPoint();
    inline int getDim();

    inline void vectorPointSumRand(Points<dimension>*,Points<dimension>,double,int,DirectionnalVector);

    inline DirectionnalVector getPreviousDirectionnalVector(int) const;
    inline void setPreviousDirectionnalVector(int,DirectionnalVector);

    inline std::string getBeforeSP();
    inline void setBeforeSP(std::string);

    inline std::string getAfterDV();
    inline void setAfterDV(std::string);


    inline void printDebug();
    friend std::ostream& operator<<(std::ostream& os, Tiles t) {
      // before, all points, all directionnal vectores after
      os << t.getBeforeSP() << "\t" << t.getSamplingPoint() << t.getPreviousRefPoint();
      for (int i = 0; i < t.getDim(); i++) {
        os << t.getPreviousDirectionnalVector(i+1);
      }
      os << t.getAfterDV() << "\n";
      return os;
    }
};

template<int dimension>
inline void Tiles<dimension>::vectorPointSumRand(Points<dimension>* point,Points<dimension> refPoint,double valToMultiplyBy,int indice,DirectionnalVector dV){
  if (dimension == 2) {
    int delta;
    if (dV.getImpDim() == 1) {
      delta = indice / 8;
    }else{
      delta = indice % 8;
    }
    point->dimensionnalArray[dV.getImpDim()-1] = (refPoint.dimensionnalArray[dV.getImpDim()-1] +dV.getImpVal()*(delta + valToMultiplyBy) / 8);
  }else{
    if (dimension == 3) {
      int delta;
      if (dV.getImpDim() == 1) {
        delta = indice / 16;
      }else{
        if (dV.getImpDim() == 2) {
          delta = ((indice/4)%4);
        }else{
          delta = indice % 4;
        }
      }
      point->dimensionnalArray[dV.getImpDim()] = (refPoint.dimensionnalArray[dV.getImpDim()-1] + dV.getImpVal()*(delta + valToMultiplyBy) / 4);
    }else{
      point->dimensionnalArray[dV.getImpDim()] = (refPoint.dimensionnalArray[dV.getImpDim()-1]+dV.getImpVal()*valToMultiplyBy);
    }
  }
}

template<int dimension>
Tiles<dimension> parseLine(int,std::string);

template<int dimension>
std::vector<Tiles<dimension>>* importTiles(std::string);

template<int dimension>
inline Tiles<dimension>::Tiles(void){}

template<int dimension>
inline Tiles<dimension>::~Tiles(){

};

template<int dimension>
inline void Tiles<dimension>::setSamplingPoint(Points<dimension> newSP){
  samplingPoint = newSP;
}

template<int dimension>
inline Points<dimension>* Tiles<dimension>::getSamplingPoint(){
  return &samplingPoint;
}

template<int dimension>
inline int Tiles<dimension>::getDim(){
  return dimension;
}

template<int dimension>
inline Points<dimension>* Tiles<dimension>::getPreviousRefPoint(){
  return &previousRefPoint;
}

template<int dimension>
inline DirectionnalVector Tiles<dimension>::getPreviousDirectionnalVector(int Dim) const{
  return previousDirectionnalVectors[Dim-1];
}

template<int dimension>
inline void Tiles<dimension>::setPreviousDirectionnalVector(int index,DirectionnalVector dV){
  previousDirectionnalVectors[index] = dV;
}

template<int dimension>
inline void Tiles<dimension>::printDebug(){
  std::cout << "Tuile " << dimension << "-D : \n Before Sampling Point : " << beforeSamplingPoint << "\t  Sampling Point : " << samplingPoint.printDebug() << "\t previousRefPoint : " << previousRefPoint.printDebug();
  for (int i = 0; i < dimension; i++) {
    std::cout << getPreviousDirectionnalVector(i+1).printDebug();
  }
   std::cout << " afterPreviousDirectionnalVectors : " << afterPreviousDirectionnalVectors;
  }

template<int dimension>
inline std::string Tiles<dimension>::getBeforeSP(){
    return beforeSamplingPoint;
}

template<int dimension>
inline void Tiles<dimension>::setBeforeSP(std::string bef){
  beforeSamplingPoint = bef;
}

template<int dimension>
inline std::string Tiles<dimension>::getAfterDV(){
    return afterPreviousDirectionnalVectors;
}

template<int dimension>
inline void Tiles<dimension>::setAfterDV(std::string aFDV){
  afterPreviousDirectionnalVectors = aFDV;
}

template<int dimension>
Tiles<dimension> parseLine(std::string  lineToParse){
  std::string afterPreviousDirectionnalVectors = "";
  double val;
  Tiles<dimension> tileToRet;
  size_t pos = 0;
  std::string token;
  // ======= beforeSamplingPoint ======= //
      // === TileType === //
      pos = lineToParse.find("\t");
      tileToRet.setBeforeSP(tileToRet.getBeforeSP()+lineToParse.substr(0,pos)+"\t");
      lineToParse.erase(0, pos + 1);

      // === Structural ID === //
      pos = lineToParse.find("\t");
      tileToRet.setBeforeSP(tileToRet.getBeforeSP()+lineToParse.substr(0,pos)+"\t");
      lineToParse.erase(0, pos + 1);

      for (int sPComponent = 0; sPComponent < dimension; sPComponent++) {
        pos = lineToParse.find("\t");
        tileToRet.getSamplingPoint()->dimensionnalArray[sPComponent] = std::stod(lineToParse.substr(0,pos));
        lineToParse.erase(0, pos + 1);
      }

      for (int sPComponent = 0; sPComponent < dimension; sPComponent++) {
        pos = lineToParse.find("\t");
        tileToRet.getPreviousRefPoint()->dimensionnalArray[sPComponent] = std::stod(lineToParse.substr(0,pos));
        lineToParse.erase(0, pos + 1);
      }

      for (int dVNb = 0; dVNb < dimension; dVNb++) {
        for (int dVComponent = 0; dVComponent < dimension; dVComponent++) {
          pos = lineToParse.find("\t");
          val = std::stod(lineToParse.substr(0,pos));
          if (val != 0) {
              tileToRet.setPreviousDirectionnalVector(dVComponent,DirectionnalVector(dimension,dVComponent+1,val));
          }
          lineToParse.erase(0, pos + 1);
        }
      }


      tileToRet.setAfterDV(tileToRet.getAfterDV()+lineToParse.substr(0,std::string::npos));
      lineToParse.erase(0, std::string::npos);
      return tileToRet;
};

template<int dimension>
std::vector<Tiles<dimension>>* importTiles(std::string fileToRead){
  std::ifstream tilesFile(fileToRead, std::ios::in);
  std::vector<Tiles<dimension>>* vectorOfTiles = new std::vector<Tiles<dimension>>;
  if (tilesFile.is_open()) {
    std::string line;
    while (getline(tilesFile,line)) {
      vectorOfTiles->push_back(parseLine<dimension>(line));
    }
    tilesFile.close();

  }else{
    std::cerr << "Unable to open file\n";
  }
  return vectorOfTiles;
}
